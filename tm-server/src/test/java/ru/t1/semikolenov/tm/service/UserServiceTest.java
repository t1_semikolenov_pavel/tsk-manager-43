package ru.t1.semikolenov.tm.service;

public class UserServiceTest {

//    @NotNull
//    private final IPropertyService propertyService = new PropertyService();
//
//    @NotNull
//    private final ITaskRepository taskRepository = new TaskRepository();
//
//    @NotNull
//    private final IProjectRepository projectRepository = new ProjectRepository();
//
//    @NotNull
//    private final IUserRepository userRepository = new UserRepository();
//
//    @NotNull
//    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);
//
//    private final static long INITIAL_SIZE = 2;
//
//    @Before
//    public void init() {
//        userService.create("test", "test", "test@test.ru");
//        userService.create("admin", "admin", Role.ADMIN);
//    }
//
//    @Test
//    public void create() {
//        Assert.assertThrows(EmptyLoginException.class, () -> userService.create("", "12345"));
//        Assert.assertThrows(ExistsLoginException.class, () -> userService.create("test", "12345"));
//        Assert.assertThrows(EmptyPasswordException.class, () -> userService.create("test-1", ""));
//        @NotNull final User user = (userService.create("test-1", "12345"));
//        Assert.assertEquals(INITIAL_SIZE + 1, userService.getSize());
//        Assert.assertEquals(Role.USUAL, user.getRole());
//    }
//
//    @Test
//    public void createWithEmail() {
//        Assert.assertThrows(EmptyLoginException.class,
//                () -> userService.create("", "12345", "test@test"));
//        Assert.assertThrows(ExistsLoginException.class,
//                () -> userService.create("test", "12345", "test@test"));
//        Assert.assertThrows(EmptyPasswordException.class,
//                () -> userService.create("test-1", "", "test@test"));
//        Assert.assertThrows(ExistsEmailException.class,
//                () -> userService.create("test-1", "12345", "test@test.ru"));
//        @NotNull final User user = (userService.create("test-1", "12345", "test-1@test"));
//        Assert.assertEquals(INITIAL_SIZE + 1, userService.getSize());
//        Assert.assertEquals(Role.USUAL, user.getRole());
//        Assert.assertNotNull(user.getEmail());
//    }
//
//    @Test
//    public void createWithRole() {
//        Assert.assertThrows(EmptyLoginException.class,
//                () -> userService.create("", "12345", Role.ADMIN));
//        Assert.assertThrows(ExistsLoginException.class,
//                () -> userService.create("test", "12345", Role.ADMIN));
//        Assert.assertThrows(EmptyPasswordException.class,
//                () -> userService.create("test-1", "", Role.ADMIN));
//        @NotNull final User user = (userService.create("test-1", "12345", Role.ADMIN));
//        Assert.assertEquals(INITIAL_SIZE + 1, userService.getSize());
//        Assert.assertEquals(Role.ADMIN, user.getRole());
//    }
//
//    @Test
//    public void findByLogin() {
//        Assert.assertThrows(EmptyLoginException.class, () -> userService.findByLogin(""));
//        Assert.assertNotNull(userService.findByLogin("test"));
//        Assert.assertNull(userService.findByLogin("test_user"));
//    }
//
//    @Test
//    public void findByEmail() {
//        Assert.assertThrows(EmptyEmailException.class, () -> userService.findByEmail(""));
//        Assert.assertNotNull(userService.findByEmail("test@test.ru"));
//        Assert.assertNull(userService.findByEmail("test_email@test.ru"));
//    }
//
//    @Test
//    public void isLoginExist() {
//        Assert.assertTrue(userService.isLoginExist("test"));
//        Assert.assertFalse(userService.isLoginExist("test_user"));
//        Assert.assertFalse(userService.isLoginExist(""));
//        Assert.assertFalse(userService.isLoginExist(null));
//    }
//
//    @Test
//    public void isEmailExist() {
//        Assert.assertTrue(userService.isEmailExist("test@test.ru"));
//        Assert.assertFalse(userService.isEmailExist("test_email@user.ru"));
//        Assert.assertFalse(userService.isEmailExist(""));
//        Assert.assertFalse(userService.isEmailExist(null));
//    }
//
//    @Test
//    public void remove() {
//        @NotNull final User user = userService.findByLogin("test");
//        userService.remove(user);
//        Assert.assertEquals(INITIAL_SIZE - 1, userService.getSize());
//        Assert.assertNull(userService.findByLogin(user.getLogin()));
//    }
//
//    @Test
//    public void removeByLogin() {
//        Assert.assertThrows(EmptyLoginException.class, () -> userService.removeByLogin(""));
//        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeByLogin("test_user"));
//        @NotNull final User user = userService.removeByLogin("test");
//        Assert.assertEquals(INITIAL_SIZE - 1, userService.getSize());
//        Assert.assertNull(userService.findByLogin(user.getLogin()));
//    }
//
//    @Test
//    public void removeByEmail() {
//        Assert.assertThrows(EmptyEmailException.class, () -> userService.removeByEmail(""));
//        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeByEmail("test_email@test.ru"));
//        @NotNull final User user = userService.removeByEmail("test@test.ru");
//        Assert.assertEquals(INITIAL_SIZE - 1, userService.getSize());
//        Assert.assertNull(userService.findByEmail(user.getEmail()));
//    }
//
//    @Test
//    public void setPassword() {
//        @NotNull final User user = userService.create("test-1", "test-1");
//        @NotNull final String oldPasswordHash = user.getPasswordHash();
//        Assert.assertThrows(EmptyIdException.class, () -> userService.setPassword("", "123"));
//        Assert.assertThrows(EmptyPasswordException.class, () -> userService.setPassword(user.getId(), ""));
//        userService.setPassword(user.getId(), "12345");
//        Assert.assertNotEquals(oldPasswordHash, user.getPasswordHash());
//    }
//
//    @Test
//    public void updateUser() {
//        @NotNull final User user = userService.create("test-1", "test-1");
//        @NotNull final String userId = user.getId();
//        Assert.assertThrows(EmptyIdException.class,
//                () -> userService.updateUser("", "", "", ""));
//        @NotNull final String firstName = "first_name";
//        @NotNull final String lastName = "last_name";
//        @NotNull final String middleName = "middle_name";
//        userService.updateUser(userId, firstName, lastName, middleName);
//        Assert.assertNotNull(user.getFirstName());
//        Assert.assertNotNull(user.getLastName());
//        Assert.assertNotNull(user.getMiddleName());
//        Assert.assertEquals(firstName, user.getFirstName());
//        Assert.assertEquals(lastName, user.getLastName());
//        Assert.assertEquals(middleName, user.getMiddleName());
//    }
//
//    @Test
//    public void lockUserByLogin() {
//        Assert.assertThrows(EmptyLoginException.class, () -> userService.removeByLogin(""));
//        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeByLogin("test_user"));
//        @NotNull final User user = userService.create("test-1", "test-1");
//        Assert.assertFalse(user.getLocked());
//        userService.lockUserByLogin(user.getLogin());
//        Assert.assertTrue(user.getLocked());
//    }
//
//    @Test
//    public void unlockUserByLogin() {
//        Assert.assertThrows(EmptyLoginException.class, () -> userService.removeByLogin(""));
//        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeByLogin("test_user"));
//        @NotNull final User user = userService.create("test-1", "test-1");
//        user.setLocked(true);
//        Assert.assertTrue(user.getLocked());
//        userService.unlockUserByLogin(user.getLogin());
//        Assert.assertFalse(user.getLocked());
//    }

}

