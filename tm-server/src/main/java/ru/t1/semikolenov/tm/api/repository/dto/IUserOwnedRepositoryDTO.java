package ru.t1.semikolenov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.semikolenov.tm.enumerated.Sort;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepositoryDTO<M extends AbstractUserOwnedModelDTO> extends IRepositoryDTO<M> {

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Sort sort);

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    void remove(@NotNull String userId, @NotNull M model);

    void removeById(@NotNull String userId, @NotNull String id);

    void clear(@NotNull String userId);

    long getCount(@NotNull String userId);

    boolean existsById(@NotNull String userId, @NotNull String id);

}
