package ru.t1.semikolenov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.dto.model.UserDTO;

public interface IUserRepositoryDTO extends IRepositoryDTO<UserDTO> {

    @Nullable
    UserDTO findOneByLogin(@NotNull String login);

    @Nullable
    UserDTO findOneByEmail(@NotNull String email);

}
