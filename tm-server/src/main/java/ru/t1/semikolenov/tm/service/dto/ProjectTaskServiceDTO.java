package ru.t1.semikolenov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.t1.semikolenov.tm.api.service.IConnectionService;
import ru.t1.semikolenov.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.t1.semikolenov.tm.dto.model.TaskDTO;
import ru.t1.semikolenov.tm.exception.entity.TaskNotFoundException;
import ru.t1.semikolenov.tm.exception.field.EmptyIdException;
import ru.t1.semikolenov.tm.exception.field.EmptyUserIdException;
import ru.t1.semikolenov.tm.repository.dto.TaskRepositoryDTO;

public class ProjectTaskServiceDTO implements IProjectTaskServiceDTO {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    private final ITaskRepositoryDTO taskRepository;

    public ProjectTaskServiceDTO(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
        this.taskRepository = new TaskRepositoryDTO(connectionService.getEntityManager());
    }

    @Override
    public void bindTaskToProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        @Nullable final TaskDTO task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        taskRepository.update(task);
    }

    @Override
    public void unbindTaskFromProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        @Nullable final TaskDTO task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        taskRepository.update(task);
    }

    @Override
    public void removeProjectById(@NotNull String userId, @NotNull String projectId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.removeTasksByProjectId(projectId);
    }

}
