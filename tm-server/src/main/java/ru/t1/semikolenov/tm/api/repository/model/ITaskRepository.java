package ru.t1.semikolenov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeTasksByProjectId(String projectId);

}
