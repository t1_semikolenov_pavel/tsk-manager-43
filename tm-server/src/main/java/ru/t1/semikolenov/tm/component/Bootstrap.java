package ru.t1.semikolenov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.api.endpoint.*;
import ru.t1.semikolenov.tm.api.service.*;
import ru.t1.semikolenov.tm.api.service.dto.IProjectServiceDTO;
import ru.t1.semikolenov.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.t1.semikolenov.tm.api.service.dto.ITaskServiceDTO;
import ru.t1.semikolenov.tm.api.service.dto.IUserServiceDTO;
import ru.t1.semikolenov.tm.endpoint.*;
import ru.t1.semikolenov.tm.service.*;
import ru.t1.semikolenov.tm.service.dto.ProjectServiceDTO;
import ru.t1.semikolenov.tm.service.dto.ProjectTaskServiceDTO;
import ru.t1.semikolenov.tm.service.dto.TaskServiceDTO;
import ru.t1.semikolenov.tm.service.dto.UserServiceDTO;
import ru.t1.semikolenov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final ITaskServiceDTO taskService = new TaskServiceDTO(connectionService);

    @Getter
    @NotNull
    private final IProjectServiceDTO projectService = new ProjectServiceDTO(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskServiceDTO projectTaskService = new ProjectTaskServiceDTO(connectionService);

    @Getter
    @NotNull
    private final IUserServiceDTO userService = new UserServiceDTO(connectionService, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final Backup backup = new Backup(this);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final CalculatorEndpoint calculatorEndpoint = new CalculatorEndpoint();

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(projectTaskEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
        registry(calculatorEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void run() {
        initPID();
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SERVER STOPPED **");
        backup.stop();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

}