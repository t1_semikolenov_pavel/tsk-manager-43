package ru.t1.semikolenov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRepositoryDTO extends IUserOwnedRepositoryDTO<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeTasksByProjectId(String projectId);

}
